tree
	namespaces
		rdf xmlns:http://www.w3.org/1999/02/22-rdf-syntax-ns file:rdf.tree
		rdfs xmlns:http://www.w3.org/2000/01/rdf-schema file:rdfs.tree
		owl xmlns:http://www.w3.org/2002/07/owl file:owl.tree
	primitive_datatypes
		Boolean a:Class regexp:`<t>|<f>`
		Integer a:Class regexp:`-?\d+([Ee]\d+)?`
		Float a:Class regexp:`-?\d+\.\d*([Ee]-?\d+)`
		String a:Class regexp:`".*?"(ru|en)?` ; eq:xsd/string
		Id a:Class regexp:`[A-Za-zА-Яа-я\-_/0-9]+`
		Year a:Class regexp:`\d{4}|<current>`
		Date a:Class regexp:`\d{2}\.\d{2}\.\d{4}|<current>`
		Url a:Classl regexp:`.+?\.(ru|su|рф|com|net|org|biz|info|cc|ws|in|tv|mobi|club|name|me|pro|no|fi|ee|lv|lt|by|ua|tk|kz|jp|us|se|de|fr|uk|gov|edu|mil)`
		URI a:Class eq:Url
		IRI a:Class eq:URI
		Path a:Class eq:String
		Regexp a:Class regexp:``.*?``
		Code a:Class regexp:``.*?``
		OdysseusCode a:Class subclass-of:Class d:"Code runnable with odysseus library"
		Comment a:Class subclass-of:String regexp:`".*?"(@(author)/.*?)?` ; TODO: define modification regexp rule scheme: by default either to complemet superclass definition or substitute it
	complex_datatypes
		YearOrDate a:Class types-combination:`(Class (union-of Date Year))`
		TimeInterval a:Class types-combination:`YearOrDate-YearOrDate`
		Literal a:Class union-of:Integer,Float,String,Year,Date,Url,Path
		Ref a:Class union-of:Id,URI,IRI
	core_syntax d:"Classes and Properties used in schema (but they can be also used in the data files)"
		classes section-hierarchy-relation:subclass-of
			Class eq:rdfs/Class
			Datatype eq:rdfs/Datatype
			Namespace a:Class
			Property eq:rdf/Property
			Resource eq:rdfs/Resource
			TabtreeSection a:Class d:"Describes the section in .tree file"
			Thing d:"Most general type of the instances considered in the project"
		properties section-hierarchy-relation:subproperty-of
			a eq:rdf/type
			abbr a:Property domain:Resource range:Id,String
			alt-id a:Property domain:Resource range:Id d:"alternative id(s)"
				id-ru d:"Id in Russian"
				id-en d:"Id in English"
				short-id
			; comment alt-id:note a:Property domain:Resource range:String
			all-values-from eq:owl/allValuesFrom
			to-authentic-format a:Property domain:Property range:OdysseusCode d:"Specifies the translation from the used property value to the format required by external standards and taxonomies, like SBO, GO etc."
			cardinality eq:owl/cardinality
			comment eq:rdfs/comment
			d a:Property alt-id:description eq:dc/description,rdfs/comment domain:Resource range:String d:"description string"
			deabbr a:Property domain:Resource range:String d:"Abbreviation meaning"
			disjoint-with eq:owl/disjointWith
			domain eq:rdfs/domain	
			; eq a:Property type:functional domain:Resource range:Resource d:"establishes equivalence between considered item and some rdf/rdfs/owl element"
			eq same-as:same-as
			file a:Property domain:Namespace range:Path d:"A file with schema description"
			instance-of eq:a
			intersection-of eq:owl/intersectionOf
			inverse-of eq:owl/inverseOf
			max-cardinality eq:owl/maxCardinality
			min-cardinality eq:owl/minCardinality
			one-of eq:owl/oneOf
			part-of a:Property domain:Resource range:Resource
			prefix a:Property domain:Namespace range:String,`(one-of <none>)`
			type a:Property domain:Resource range:`(one-of functional inverse_functional symmetric object datatype)`
			range subproperty-of:rdfs/range range:Class,Code
			regexp domain:Datatype range:Regexp d:"A regular expression that defines a range of possible symbols and their combinations for scalar datatypes"
			ref domain:Resource range:Boolean d:"Shows that the entity is described somewhere else"
			rel a:Property domain:Resource range:Resource d:"Establishes relation between Object and Subject"
			; TODO: at reading, semantic should be following: first read all properties from the class pointed by owl/sameAs, and then overwrite them by properties pointed directly in the definition line. In this case type owl/Property will be overwrited by type owl/SymmetricProperty
			section-root-class a:Property domain:TabtreeSection range:Class d:"Defines the common superclass for all the classes declared in the section"
			same-as a:owl/SymmetricProperty owl/sameAs:owl/sameAs 
			section-class a:Property domain:TabtreeSection range:Class d:"Defines default classes of the items in subsequent hierarchy" example:"For section 'classes' it would be 'section-class:Class', for section 'properties' it would be 'section-class:Property'"
			section-hierarchy-relation a:Property domain:TabtreeSection range:Property d:"Defines relations between elements in subsequent hierarchy that follows current section in .tree file. Relation goes from one element (subject) to another element (object), that is always exactly one level up in hierarchy"
			spec a:Property domain:Namespace range:Path
			subclass-of alt-id:is-a a:Property eq:rdfs/subClassOf
			subproperty-of eq:rdfs/subPropertyOf
			types-combination a:Property type:functional domain:Datatype range:Code
			union-of a:Property domain:Datatype range:Datatype
			xmlns a:Property domain:Namespace range:Url