#lang racket

(define MROT (hash '2016 6204 '2017 7500)
(define MEDSTR (hash '2019 0.051))
(define PENSTR (hash '2019 0.26))

(define income ...)
(define MoeDelo_payment (* 833 12))
(define Tochka_payment ...)
(define income ...)
(define curyear '2019)

(define Medstr (* (hash-ref PENSTR curyear) 12 (hash-ref MROT curyear)))
(define Penstr (* (hash-ref PENSTR curyear) 12 (hash-ref MROT curyear)))
(define Penstr_extra (cond
                      ((> income 300000) (* 0.01 income))
                      (else 0)))

(define deadlines (hash 'Penstr_extra "01.04.20xx"))

(define fixed_payment (+ Medstr (+ Penstr Penstr_extra)))
(define Usn (let* ((usn (* 0.06 income))
                  (usn (cond
                            ((> usn fixed_payment) (- usn fixed_payment))
                            (else 0))))
              usn))

(define all_tax_payments (+ Usn fixed_payment))
(define services_payment (+ MoeDelo_payment Tochka_payment))

(define total_payment all_tax_payments services_payment)
